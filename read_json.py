import json
import pandas as pd
from pandas.io.json import json_normalize #package for flattening json in pandas df

#load json object
with open('challenge.json') as f:
    data = json.load(f)

normalize_data = json_normalize(data[39])
columns = normalize_data.columns.values
print("--------Main structured table: ")
print(normalize_data.dtypes)

def findOneMany(columns):
    for column in columns:
        for i in range(len(normalize_data[column])):
            if isinstance(normalize_data[column][i], list) and len(normalize_data[column][i]) != 0:
                print('---------' + column + ' relationship 1-n with structured table: ')
                extractList(normalize_data[column][i])

def extractList(list):
    for element in list:
        normalize_data = json_normalize(element)
        print(normalize_data.dtypes)
        if isinstance(normalize_data, type(list)):
            extractList()

findOneMany(columns)

